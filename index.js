var express = require("express");
var todoRoute = require("./routes/todoRoute");
var todoArrayRoute = require("./routes/todosArrayRoute");
var app = express();

app.use(express.json());
// app.use(todoRoute);
app.use(todoArrayRoute);
app.listen("5000", (err) => {
  if (err) {
    console.log(err);
  }
  console.log("Server is up and running at prot 5000");
});
