var express = require("express");
var app = express();
const axios = require("axios");
app.use(express.json());
var router = express.Router();

module.exports = router.post("/todoArrayData", async (req, res) => {
  /*
  dataFormat -->
  [
    {
        "Employee":
  {
    "id": "101"
  }
  },
   {
        "Employee":
  {
    "id": "102"
  }
  }

]
  */
  let data = req.body;
  let data_array = [];
  let object1 = [];
  for (i = 0; i < data.length; i++) {
    data_array.push(data[i].Employee.id);
  }
  let result = await axios.get("https://jsonplaceholder.typicode.com/todos/", {
    params: { id: data_array },
  });
  for (i = 0; i < result.data.length; i++) {
    object1.push(
      Object.assign((({ id, title }) => ({ id, title }))(result.data[i]), {
        status: "Added records successfully",
      })
    );
  }
  return res.send(object1);
});
