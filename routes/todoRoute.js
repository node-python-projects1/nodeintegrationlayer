var express = require("express");
var app = express();
const axios = require("axios");
app.use(express.json());
var router = express.Router();

module.exports = router.post("/todoData", async (req, res) => {
  /*
  dataFormat -->
  {
  "Employee": {
      "PersonalInformation": { "id": "19" }
    },
  }
  */

  id = req.body.Employee.PersonalInformation.id;

  //https://jsonplaceholder.typicode.com/todos/5
  //https://dummy.restapiexample.com/api/v1/employee/
  await axios
    .get(`https://jsonplaceholder.typicode.com/todos/${id}`)
    .then((response) => {
      var newResponse = response.data;
      var object1 = (({ id, title }) => ({ id, title }))(newResponse);
      object2 = Object.assign({ status: "Successfully added record" }, object1);
      return res.status(200).send(JSON.stringify(object2));
    })
    .catch((error) => {
      return res.status(422).send(error);
    });
});
